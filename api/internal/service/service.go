package service

import (
	"net/http"
	"teste-mongo/internal/handler"
)

func StartService() {
	http.HandleFunc("/user/create", handler.CreateUser)
	http.HandleFunc("/user/list", handler.ListUsers)
	http.ListenAndServe(":8080", nil)
}
