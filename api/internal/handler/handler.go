package handler

import (
	"context"
	"encoding/json"
	"net/http"
	"teste-mongo/internal/model"
	"teste-mongo/internal/repository"

	"go.mongodb.org/mongo-driver/bson"
)

func CreateUser(res http.ResponseWriter, req *http.Request) {

	res.Header().Set("Access-Control-Allow-Origin", "*")
	res.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
	res.Header().Set("Access-Control-Allow-Headers", "Content-Type, Authorization")

	client := repository.StartRepository()
	collection := client.Database("teste-mongo").Collection("usuarios")

	var user model.User
	_ = json.NewDecoder(req.Body).Decode(&user)

	result, err := collection.InsertOne(context.TODO(), user)
	if err != nil {
		panic(err)
	}

	json.NewEncoder(res).Encode(result)
}

func ListUsers(res http.ResponseWriter, req *http.Request) {

	res.Header().Set("Access-Control-Allow-Origin", "*")
	res.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
	res.Header().Set("Access-Control-Allow-Headers", "Content-Type, Authorization")

	var users []model.User
	client := repository.StartRepository()
	collection := client.Database("teste-mongo").Collection("usuarios")

	result, err := collection.Find(context.TODO(), bson.M{})
	if err != nil {
		panic(err)
	}

	for result.Next(context.TODO()) {
		var user model.User
		err := result.Decode(&user)
		if err != nil {
			http.Error(res, err.Error(), http.StatusInternalServerError)
			return
		}

		users = append(users, user)
	}

	json.NewEncoder(res).Encode(users)
}
