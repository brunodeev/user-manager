package main

import (
	"teste-mongo/internal/repository"
	"teste-mongo/internal/service"
)

func main() {
	repository.StartRepository()
	service.StartService()
}
